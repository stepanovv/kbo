# IDE

 * https://microsoft.github.io/language-server-protocol/implementors/servers/
 * mac - http://www.barebones.com/
 * https://github.com/eranif/codelite
 * https://github.com/elementary/code#readme
 * https://sourcehut.org/
 * embedded web - https://ide.sourcelair.com/home
 * CSV editor, and large file viewer for Windows - https://www.emeditor.com/
 * https://wiki.gnome.org/Apps/Builder/
 * https://api.kde.org/frameworks/ktexteditor/html/
 * vi
	* https://github.com/mawww/kakoune
 * secured airgap - https://github.com/ActiveState/OpenKomodoIDE
 * https://codeberg.org/JakobDev/jdTextEdit